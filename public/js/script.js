$(document).ready(function(){

$('.editor').summernote({
    height: 150,
    tabsize: 2,
    toolbar: [
      // [groupName, [list of button]]
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['strikethrough', 'superscript', 'subscript']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']]
    ]
  });



$('#frm_produk').on('submit',function(e){
	e.preventDefault();
	var frm_data = $(this).serialize();
	var url = $(this).attr('action');
	var method = $(this).attr('method');
	$.ajax({
		url:url,
		type:method,
		dataType:'JSON',
		data:frm_data,
		success:function(result){
			$('#cover_upload_gambar_produk').show();
			$('.cover_frm_produk').hide();
			// alert(result.previous_url);
			// $(".fb_frm_produk").html("<div class='alert alert-info'>"+result.pesan+"<div>");
			// window.location = result.previous_url;

		},
		error:function(result){
			console.log(result.responseText);
			// alert('Terjadi Kesalahan !');
		}
	});
});

// $('#frm_editproduk').on('submit',function(e){
// 	e.preventDefault();
// 	var url 	= $(this).attr('action');
// 	var method 	= $(this).attr('method');
// 	var data 	= $(this).serialize();
// 	// alert(url);
// 	$.ajax({
// 		url : url,
// 		type : method,
// 		dataType : 'JSON',
// 		data:data,
// 		success:function(result){
// 			$('#fb_editproduk').html(result.pesan);
// 			var time = 2;
// 			setInterval(function(){
// 			 if(time<=0)
// 			 {
// 			 	$('#fb_editproduk').html('');
// 			 }
// 			 time--;
// 			}, 1000);
// 		},
// 		error:function(result){
// 			var errors = result.responseJSON['errors'];
// 			$('#nama_produkError').html(errors.nama_produk);
// 			$('#harga_produkError').html(errors.harga_produk);
// 			$('#kategori_produkError').html(errors.kategori_produk);
// 			$('#deskripsi_produkError').html(errors.deskripsi_produk);
// 		}
//
// 	});
// });
});

function hapus_produk(kode_produk)
{
	var url = $('#'+kode_produk).attr('href');

	$.ajax({
		url:url,
		method:'post',
		success:function(result)
		{
			location.reload();
		},
		error:function(result){

		}
	});
}
