<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['created_at'];

    public static function get_all()
    {
      return DB::table('products')
            ->join('penggunas', 'products.username', '=', 'penggunas.username')
            ->join('categories', 'products.kode_kategori', '=', 'categories.kode_kategori')
            ->select('*')
            ->get();
    }

    public static function get_all_by_category($category, $cari, $username)
    {
      DB::enableQueryLog();
      $products = DB::table('products')
          ->join('penggunas', 'products.username', '=', 'penggunas.username')
          ->join('categories', 'products.kode_kategori', '=', 'categories.kode_kategori')
          ->select('*')
          ->orderBy('kode_produk', 'desc');

      if(!empty($category)){
        $products = $products->where('categories.nama_kategori', $category);
      }

      if(!empty($cari)){
        $products = $products->where('products.nama_produk', 'like', '%'.$cari.'%');
      }

      if(!empty($username)){
        $products = $products->where('products.username', $username);
      }

      return $products->get();

      // return DB::getQueryLog();
    }

    public static function get_latest()
    {
      return DB::table('products')
            ->orderBy('kode_produk', 'desc')
            ->limit(4)
            ->get();
    }

    public static function get_by_category($kode_kategori)
    {
      return DB::table('products')
            ->where('kode_kategori', $kode_kategori)
            ->orderBy('kode_produk', 'desc')
            ->limit(4)
            ->get();
    }

    public static function get_by_username($username)
    {
      return DB::table('products')
            ->join('categories', 'products.kode_kategori', '=', 'categories.kode_kategori')
            ->select('*')
            ->where('username', $username)
            ->get();
    }

    public static function get_by_code($kode_produk)
    {
      return DB::table('products')
            ->join('penggunas', 'products.username', '=', 'penggunas.username')
            ->join('categories', 'products.kode_kategori', '=', 'categories.kode_kategori')
            ->select('*')
            ->where('products.kode_produk', $kode_produk)
            ->first();
    }

    public static function get_num_product()
    {
      return DB::select("SELECT penggunas.nama_toko, products.username, Count(kode_produk) AS banyak
        FROM products INNER JOIN penggunas
        ON products.username = penggunas.username
        GROUP BY products.username, penggunas.nama_toko");
    }
}
