<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Categorie;

class CategoryController extends Controller
{
    public function index()
    {
      if(!Session::get('username') or Session::get('status') <> 'Admin')
  		{
  		    return redirect()->to('/');
  		}

      $data['categorylist'] = Categorie::all();
      return view('admin/Category', $data);
    }

    public function categoryform($kode_kategori)
    {
      $data['kode_kategori']    = $kode_kategori;

      if($data['kode_kategori'] == "new"){
        $data['nama_kategori']  = "";
      }else{
        $data_category          = Categorie::where('kode_kategori', $data['kode_kategori'])->first();
        $data['nama_kategori']  = $data_category->nama_kategori;
      }

      return view('admin/CategoryForm', $data);
    }

    public function categorydelete($kode_kategori, Request $request)
    {
      $data_category          = Categorie::where('kode_kategori', $kode_kategori)->first();
      Categorie::where('kode_kategori', $kode_kategori)->delete();
      
      $request->session()->flash('info', 'Data <strong>'.$data_category->nama_kategori.'</strong> telah dihapus!');
      $request->session()->flash('kelas', 'warning');
      return redirect('admin/categorylist.html');
    }

    public function store(Request $request)
    {
      $request->session()->flash('kelas', 'info');
      $vali['nama_kategori']  = 'required';
      $msg_error['required']  = 'Tidak boleh kosong';

      $this->validate($request,$vali,$msg_error);

      $data['kode_kategori']  = $request->kode_kategori;
      $data['nama_kategori']  = $request->nama_kategori;

      if(!empty($data['kode_kategori']))
      {
        $request->session()->flash('info', 'Data <strong>'.$data['nama_kategori'].'</strong> berhasil diperbarui!');
        Categorie::where('kode_kategori',$data['kode_kategori'])->update($data);
      }else{
        $request->session()->flash('info', 'Data <strong>'.$data['nama_kategori'].'</strong> berhasil disimpan!');
        Categorie::create($data);
      }

      return redirect()->back();
    }
}
