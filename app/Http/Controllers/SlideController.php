<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Slide;

class SlideController extends Controller
{
    public function index()
    {
      if(!Session::get('username') or Session::get('status') <> 'Admin')
  		{
  		    return redirect()->to('/');
  		}

      $data['slidelist'] = Slide::all();
      return view('admin/Slide', $data);
    }

    public function slideform($id)
    {
      $data['id']       = $id;

      if($data['id']    == "new"){
        $data['slide']  = "";
      }else{
        $data_slide     = Slide::where('id', $id)->first();
        $data['slide']  = $data_slide->slide;
      }

      return view('admin/SlideForm', $data);
    }

    public function slidedelete($id, Request $request)
    {
      $data_slide      = Slide::where('id', $id)->first();
      Slide::where('id', $id)->delete();
      $request->session()->flash('info', 'Data <strong>'.$data_slide->slide.'</strong> telah dihapus!');
      return redirect('admin/slidelist.html');
    }

    public function store(Request $request)
    {
      $data = $request->image;
      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);
      $data = base64_decode($data);
      $image_name= date("Ymdhis") .'.png';
      $path = public_path() . "/images/slide/" . $image_name;

      file_put_contents($path, $data);

      Slide::create(['slide'=>$image_name]);
      $feedback['url'] = url('/admin/slidelist.html');
      return $feedback;
    }
}
