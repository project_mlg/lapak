<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Pengguna;
use App\Models\Categorie;
use App\Models\Slide;
use Illuminate\Support\Facades\DB;

class MainControl extends Controller
{
    public function index()
    {
    	$data['product']   = Product::get_latest();
      $data['categorie'] = Categorie::all();
      $data['foto']      = array();
      $data['pro_cat']   = array();
      $data['foto_cat']  = array();
    	$data['slide']     = Slide::all();

      foreach ($data['product'] as $produk) {
        $data['foto'][$produk->kode_produk]          = Pengguna::photo('_produk', $produk->foto_produk);
      }

      foreach ($data['categorie'] as $category) {
        $data['pro_cat'][$category->kode_kategori]   = Product::get_by_category($category->kode_kategori);

        foreach ($data['pro_cat'][$category->kode_kategori] as $photo_cat) {
          $data['foto_cat'][$photo_cat->kode_produk] = Pengguna::photo('_produk', $photo_cat->foto_produk);
        }
      }

    	return view('home', $data);
    }

    public function product($username, Request $request)
    {
      if($username == 'all')
      {
        $data['product']    = Product::get_all_by_category($request->kategori, $request->cari, "");
        $data['nama_bedag'] = 'Semua Produk';
      }else{
        $data['product']    = Product::get_all_by_category($request->kategori, $request->cari, $username);
        $data['nama_bedag'] = 'Produk '.Pengguna::where('username', $username)->first()->nama_toko;
      }

      $data['foto']         = array();
      $data['categorie']    = Categorie::all();
      $data['bedag']        = Product::get_num_product();

      foreach ($data['product'] as $produk) {
        $data['foto'][$produk->kode_produk]          = Pengguna::photo('_produk', $produk->foto_produk);
      }

      return view('Produk', $data);
    }
}
