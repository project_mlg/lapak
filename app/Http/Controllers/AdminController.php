<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Pengguna;

class AdminController extends Controller
{
    public function index()
    {
  		if(!Session::get('username') or Session::get('status') <> 'Admin')
  		{
  		    return redirect()->to('/');
  		}
      return view('admin/Dashboard');
    }

    public function gugu(Request $request)
    {
      echo Pengguna::unpin($request->key);
    }
}
