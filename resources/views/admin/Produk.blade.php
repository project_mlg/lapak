@extends('admin.Sidebar', ['dashboard' => '',
                            'produk' => 'active',
                            'kategori' => '',
                            'member' => '',
                            'slide' => '',
                            'profil' => '',
                            'password' => ''])

@section('title')
  Member List
@endsection

@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Produk
      <small>Daftar</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li><i class="fa fa-dropbox"></i> Produk</li>
      <li class="active">Daftar Produk</li>
    </ol>
  </section>

  <section class="content">
    @if (Session::has('info') or count($errors) > 0)
    <div class="callout callout-{{ session('kelas') }} hidden" id="information">
        <h4>Informasi</h4>
        {!! session('info') !!}
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </div>
    @endif
    <div class="row">
      <section class="col-md-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-users"></i>

            <h3 class="box-title">Produk</h3>
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Toko</th>
                        <th>Nama Produk</th>
                        <th>Kategori</th>
                        <th>Harga</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  @foreach($productlist as $product)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $product->nama_toko }}</td>
                        <td>{{ $product->nama_produk }}</td>
                        <td>{{ $product->nama_kategori }}</td>
                        <td>Rp. {{ number_format($product->harga_produk,0,'','.') }},-</td>
                        <td>
                          <button type="button" class="btn btn-success btn-sm" title="Info" data-toggle="modal" data-target="#modal-info" onclick="info('{{ $product->kode_produk }}', '{{ url('/admin/productinfo') }}')">
                            <i class="fa fa-eye"></i> View</button>

                          <button type="button" class="btn btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#modal-warning" onclick="hapus('{{ $product->nama_produk }}', '{{ url('/admin/productdelete/'.$product->kode_produk) }}')">
                            <i class="fa fa-times"></i> Delete</button>
                        </td>
                    </tr>
                    <?php $no++; ?>
                  @endforeach
                </tbody>
            </table>
          </div>
          <div class="box-footer clearfix">
          </div>
        </div>

      </section>
    </div>
  </section>
</div>



<div class="modal fade" id="modal-info">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Info Produk <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#info" data-toggle="tab">Info Produk</a></li>
            <li><a href="#foto" data-toggle="tab">Foto Produk</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="info">
              <div class="row pad-bottom-10">
                <div class="col-sm-4">
                  Pemilik
                </div>
                <div class="col-sm-8">
                  <span id="txt_pemilik_produk"></span>
                </div>
              </div>

              <div class="row pad-bottom-10">
                <div class="col-sm-4">
                  Nama Toko
                </div>
                <div class="col-sm-8">
                  <span id="txt_toko_produk"></span>
                </div>
              </div>

              <div class="row pad-bottom-10">
                <div class="col-sm-4">
                  Nama Produk
                </div>
                <div class="col-sm-8">
                  <strong id="txt_nama_produk"></strong>
                </div>
              </div>

              <div class="row pad-bottom-10">
                <div class="col-sm-4">
                  Harga Produk
                </div>
                <div class="col-sm-8">
                  <strong id="txt_harga_produk"></strong>
                </div>
              </div>

              <div class="row pad-bottom-10">
                <div class="col-sm-4">
                  Kategori Produk
                </div>
                <div class="col-sm-8">
                  <span id="txt_kategori_produk"></span>
                </div>
              </div>

              <div class="row pad-bottom-10">
                <div class="col-sm-4">
                  Deskripsi Produk
                </div>
                <div class="col-sm-8">
                  <span id="txt_deskripsi_produk"></span>
                </div>
              </div>
            </div>

            <div class="tab-pane" id="foto">
              <div class="row">
                <div class="col-md-12">
                  <center>
                    <img class="img-responsive" id="txt_foto_produk" src="{{ asset('images/_produk/default.png') }}" alt="Foto Produk" width="65%">
                  </center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal modal-primary fade" id="modal-warning">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Produk <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <p>Apakah benar, data produk <strong><span class="nama"></span></strong> akan dihapus?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
        <a href="{{ url('/admin/delete') }}" class="btn btn-danger" id="btn_delete">Delete</a>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
@endsection
