<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BedagMalang | @yield('title')</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/ion-icon.css') }}">
  <link rel="stylesheet" href="{{ asset('css/AdminLTE.css') }}">
  <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('css/iCheck/flat/blue.css') }}">
  <link rel="stylesheet" href="{{ asset('css/morris/morris.css') }}">
  <link rel="stylesheet" href="{{ asset('css/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" href="{{ asset('css/datepicker/datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('css/select2/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/daterangepicker/daterangepicker-bs3.css') }}">
  <link rel="stylesheet" href="{{ asset('css/iCheck/all.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/datatables/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('css/datatables/DataTables-1.10.16/css/dataTables.bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/dropzone/dist/dropzone.css') }}">
  <link rel="stylesheet" href="{{asset('css/croppie.css')}}">

  <script src="{{ asset('js/jquery.js') }}"></script>
  <script src="{{ asset('js/jquery-ui.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('css/datatables/DataTables-1.10.16/js/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('css/datatables/DataTables-1.10.16/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('css/iCheck/icheck.min.js') }}"></script>
  <script src="{{ asset('css/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
  <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
  <script src="{{ asset('js/fastclick.js') }}"></script>
  <script src="{{asset('js/croppie.js')}}"></script>

</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="{{ url('/admin.html') }}" class="logo">
      <span class="logo-mini"><b>B</b>M</span>
      <span class="logo-lg">BEDAG<b>MALANG</b></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ session('foto') }}" class="user-image" alt="{{ session('username') }}">
              <span class="hidden-xs">{!! session('nama_lengkap') !!}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="{{ session('foto') }}" class="img-circle" alt="{{ session('username') }}">
                <p>
                  {{ session('nama_lengkap') }}
                  <small>{{ session('status') }}</small>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('/admin/profile/'.session('username').'.html') }}" class="btn btn-default btn-flat">Profil</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/admin/signout.html') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  @yield('menu')

  <label id="error" style="color: Red; display: none; margin-bottom: 10px">* Input number</label>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      Theme by <strong><a href="http://almsaeedstudio.com">Almsaeed Studio</a></strong> <b>Version</b> 1.3.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Asoy Corp</a>.</strong> All rights reserved.
  </footer>
</div>

@if (Session::has('info') or count($errors) > 0)
  <script type="text/javascript">
    $(document).ready(function(){
      $("#information").removeClass("hidden");
      $("#information").addClass("animated");
      $("#information").addClass("fadeIn");
      setTimeout(function () {
        $("#information").addClass("animated");
        $("#information").addClass("fadeOut");
          }, 4000
      );
      setTimeout(function () {
        $("#information").addClass("hidden");
          }, 4500
      );
    });
  </script>
@endif
<script src="{{ asset('js/app.min.js') }}"></script>
<script src="{{ asset('js/demo.js') }}"></script>
<script src="{{ asset('js/dashboard.js') }}"></script>

<script>
  $(document).ready(function(){

  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
  });
    
    $image_crop = $('.cropfoto_member').croppie({
        enableExif: true,
        viewport: {
          width:220,
          height:270,
          type:'square' //circle
        },
        boundary:{
          width:300,
          height:350
        }
      });

    $crop_slide = $('.cropfoto_slide').croppie({
        enableExif: true,
        viewport: {
          width:980,
          height:450,
          type:'square' //circle
        },
        boundary:{
          width:1000,
          height:480  
        }
      });


    $('#foto_member').on('change', function(){
        // $('.image_demo').show();
        // $('#crop').show();
        var reader = new FileReader();
        reader.onload = function (event) {
          $image_crop.croppie('bind', {
            url: event.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
        // $('#uploadimageModal').modal('show');
    });

    $('#foto_slide').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
          $crop_slide.croppie('bind', {
            url: event.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
    });



    $('#uploadfoto_member').on('click', function (ev) {
    var url = $('#urlfoto_member').val();
    var username = $('#username').val();
    $image_crop.croppie('result', {
    type: 'canvas',
    size: 'viewport'
    }).then(function (resp) {
    $.ajax({
    url: url,
    type: "POST",
    data: {"image":resp,"username":username},
    success: function (result) {
      alert(result);
      // window.location = data.url;
    },
    error:function(result){
      console.log(result.responseText);
    }
    });
    });
    });

    $('#uploadfoto_slide').on('click', function (ev) {
    var url = $('#urlfoto_slide').val();
    $crop_slide.croppie('result', {
    type: 'canvas',
    size: 'viewport'
    }).then(function (resp) {
    $.ajax({
    url: url,
    type: "POST",
    data: {"image":resp},
    success: function (result) {
      window.location = result.url;
      // window.location = data.url;
    },
    error:function(result){
      console.log(result.responseText);
    }
    });
    });
    });

    $('#frm_member').on('submit', function(e){
      e.preventDefault();
      var url = $(this).attr("action");
      $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
      }).then(function (resp) {
        var file_foto = $('#foto_member').val();
        if(file_foto==""){
          $('#foto').val('');
        }else{          
          $('#foto').val(resp);
        }
        var form_data = $('#frm_member').serialize();
      $.ajax({
      url: url,
      type: "POST",
      data: form_data,
      success: function (result) {
       if(result.status == 0){
        alert(result.pesan);
        $('#fbform_member').html(result.pesan);
       }else{
        window.location = result.url;
       }
      },
      error:function(result){
        console.log(result.responseText);
      }
      });
      });
    })


  });
</script>


</body>
</html>
