@extends('layout.MainLayout')
@section('menu-akun')
@if(Session::get('username')==null)
<div class="shop-menu pull-right">
	<ul class="nav navbar-nav">
		<li><a href="{{url('/login.html')}}"><i class="fa fa-lock"></i> Login</a></li>
	</ul>
</div>
@else
<div class="shop-menu pull-right">
	<ul class="nav navbar-nav">
		<li><a href="{{url('/member/'.Session::get('username'))}}"><i class="fa fa-user"></i> {{Session::get('nama_lengkap')}}</a></li>
		<li><a href="{{url('/signout')}}"><i class="fa fa-lock"></i> Sign Out</a></li>
	</ul>
</div>
@endif
@endsection
@section('menu-web')
<?php
	$url = url()->full();
	if(strpos($url, 'produk') > 0){
	}else{
		$url = url('produk/all');
	}

	if(strpos($url, "?") > 0){
		$url = substr($url, 0, strpos($url, "?"));
	}
?>
<div class="col-sm-8">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="mainmenu pull-left">
		<ul class="nav navbar-nav collapse navbar-collapse">
			<li ><a href="{{url('/')}}" class="{{ $beranda }}">Beranda</a></li>
			<li class="dropdown"><a href="#" class="{{ $produk }}">Kategori<i class="fa fa-angle-down"></i></a>
                <ul role="menu" class="sub-menu">
										<li><a href="{{ url('produk/all' )}}">Semua Produk</a></li>
					@foreach($categorie as $kategori)
                    <li><a href="{{ url('produk/all'.'?kategori='.$kategori->nama_kategori )}}">{{$kategori->nama_kategori}}</a></li>
					@endforeach
                </ul>
            </li>
			<li><a href="{{url('/daftarbedag')}}" class="{{ $bedag }}">Daftar Bedag Malang</a></li>
		</ul>
	</div>
</div>

<div class="col-sm-4">
	<div class="search_box pull-right">
		<form action="{{ $url }}" method="get">
		<input type="hidden" name="kategori" value="<?php if(isset($_GET['kategori'])){ echo $_GET['kategori']; } ?>">
		<input type="text" name="cari" placeholder="Cari Nama Produk"/>
		</form>
	</div>
</div>
@endsection

@section('content-MainLayout')
@yield('slide')
@yield('content-PenjualanLayout')
@endsection
