<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  	<meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') BEDAGMALANG</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
  	<link href="{{asset('css/main.css')}}" rel="stylesheet">
  	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/datatables/datatables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/datatables/DataTables-1.10.16/css/dataTables.bootstrap.css') }}">
  	<link rel="stylesheet" href="{{asset('css/croppie.css')}}">
  	<link rel="stylesheet" href="{{asset('css/summernote.css')}}">
  	<link href="{{asset('css/style.css')}}" rel="stylesheet">


</head>

<body>
	<header id="header"><!--header-->

		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<h3>BEDAG<span>MALANG</span></h3>
						</div>
					</div>
					<div class="col-sm-8">
						@yield('menu-akun')
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					@yield('menu-web')
				</div>
			</div>
		</div>
	</header>



@yield('content-MainLayout')




	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="single-widget logo">
							<h2>BEDAG<span>MALANG</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="single-widget">
							<h2>Tentang BEDAG<span class="mlg">MALANG</span></h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Company Information</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Store Location</a></li>
								<li><a href="#">Affillate Program</a></li>
								<li><a href="#">Copyright</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>

    <label id="error" style="color: Red; display: none; margin-bottom: 10px">* Input number</label>
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 Asoy Corp. All rights reserved.</p>
				</div>
			</div>
		</div>

	</footer><!--/Footer-->



    <script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('css/datatables/DataTables-1.10.16/js/jquery.dataTables.js') }}"></script>
  	<script src="{{ asset('css/datatables/DataTables-1.10.16/js/dataTables.bootstrap.js') }}"></script>
	<script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('js/price-range.js')}}"></script>
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>

    <script>
    $(document).ready(function(){
    $.ajaxSetup({
	headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});



    	$('.cover_frm_produk').hide();
    	$('#cover_upload_gambar_produk').hide();

    	$('#btn_addProduk').click(function(){
			$('.cover_frm_produk').show();
			$(this).hide();
    	});



    	$('#list_produk').DataTable();


    	$('.image_demo').hide();
    	$('.gbr_produk').hide();
    	$('#crop').hide();
    	$('#crop_gambar_produk').hide();
    	$('#crop_editproduk').hide();
    	$('#crop_gbrproduk').hide();

    	$image_crop = $('.image_demo').croppie({
		    enableExif: true,
		    viewport: {
		      width:220,
		      height:270,
		      type:'square' //circle
		    },
		    boundary:{
		      width:300,
		      height:350
		    }
		  });

    	$image_crop_produk = $('.image_demo_produk').croppie({
		    enableExif: true,
		    viewport: {
		      width:220,
		      height:270,
		      type:'square' //circle
		    },
		    boundary:{
		      width:300,
		      height:350
		    }
		  });

    	$image_crop_editproduk = $('.gbr_produk').croppie({
		    enableExif: true,
		    viewport: {
		      width:220,
		      height:270,
		      type:'square' //circle
		    },
		    boundary:{
		      width:300,
		      height:350
		    }
		  });


    	$('#upload_image').on('change', function(){
    		$('.image_demo').show();
    		$('#crop').show();
    		// $('#upload_image2').click();
		    var reader = new FileReader();
		    reader.onload = function (event) {
		      $image_crop.croppie('bind', {
		        url: event.target.result
		      }).then(function(){
		        console.log('jQuery bind complete');
		      });
		    }
		    reader.readAsDataURL(this.files[0]);
		    // $('#uploadimageModal').modal('show');
		  });

    	$('#upload_image_produk').on('change', function(){
    		$('.image_demo_produk').show();
    		$('#crop_gambar_produk').show();
    		// $('#upload_image2').click();
		    var reader = new FileReader();
		    reader.onload = function (event) {
		      $image_crop_produk.croppie('bind', {
		        url: event.target.result
		      }).then(function(){
		        console.log('jQuery bind complete');
		      });
		    }
		    reader.readAsDataURL(this.files[0]);
		    // $('#uploadimageModal').modal('show');
		  });

    	$('#upload_imageEditProduk').on('change', function(){
    		$('.gbr_produk').show();
    		$('#crop_gbrproduk').show();
    		// $('#upload_image2').click();
		    var reader = new FileReader();
		    reader.onload = function (event) {
		      $image_crop_editproduk.croppie('bind', {
		        url: event.target.result
		      }).then(function(){
		        console.log('jQuery bind complete');
		      });
		    }
		    reader.readAsDataURL(this.files[0]);
		    // $('#uploadimageModal').modal('show');
		  });

    	$('#crop').on('click', function (ev) {
		var username = $('#username').val();
		var url = $('#url_fotoPengguna').val();
		$image_crop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
		}).then(function (resp) {
		$.ajax({
		url: url,
		type: "POST",
		data: {"image":resp},
		success: function (data) {
		  window.location = data.url;
		}
		});
		});
		});

		$('#crop_gambar_produk').on('click', function (ev) {
		var username = $('#username').val();
		var url = $('#url_fotoProduk').val();
		$image_crop_produk.croppie('result', {
		type: 'canvas',
		size: 'viewport'
		}).then(function (resp) {
		$.ajax({
		url: url,
		type: "POST",
		data: {"image":resp,"username":username},
		success: function (data) {
		  window.location = data.url;
		}
		});
		});
		});

		$('#crop_gbrproduk').on('click', function (ev) {
		var kode_produk = $('#kode_produk').val();
		var url = $('#url_editprodukgbr').val();
		$image_crop_editproduk.croppie('result', {
		type: 'canvas',
		size: 'viewport'
		}).then(function (resp) {
		$.ajax({
		url: url,
		type: "POST",
		data: {"image":resp,"kode_produk":kode_produk},
		success: function (data) {
		  window.location = data.url;
		}
		});
		});
		});


$('#frm_gantipassword').on('submit',function(e){
	e.preventDefault();
	$('#pass_lamaError').html("");
	$('#pass_baruError').html("");
	var url 	= $(this).attr('action');
	var method 	= $(this).attr('method');
	var data 	= $(this).serialize();
	// alert(url);
	$.ajax({
		url:url,
		method:method,
		dataType:'JSON',
		data:data,
		success:function(result){
			$('#fb_gantipassword').html(result.pesan);
			var time = 1;
			setInterval(function(){
			 if(time<=0)
			 {
			 	window.location = result.url;
			 }
			 time--;
			}, 3000);
		},
		error:function(result){
			var errors = result.responseJSON['errors'];
			$('#pass_lamaError').html(errors.pass_lama);
			$('#pass_baruError').html(errors.pass_baru);
		}
	});
});





    });
    </script>

    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/croppie.js')}}"></script>
    <script src="{{asset('js/summernote.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>
</html>
