@extends('layout.PenjualanLayout', ['beranda' => '',
                            'produk' => '',
                            'bedag' => ''
                            ])

@section('content-PenjualanLayout')
<section>

<div class="container">
	<div class="row">
		<div class="col-sm-12">

		<div class="category-tab shop-details-tab" style="margin:auto"><!--category-tab-->
			<div class="col-sm-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#details" data-toggle="tab">Biodata</a></li>
					<li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_gantipassword">Ubah Password</a></li>
					<li><a href="#profiltoko" data-toggle="tab">Daftar Produk Anda</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="details" >
					<div class="col-sm-4">
						<div class="row" id="photo_thumb">
							<img src="{{ $foto }}" class="img img-responsive " width="70%" alt="{{$pengguna->nama_lengkap}}">
						</div>
						<div class="row" style="padding-top:20px">
							<div class="col-sm-12">
								<b>Edit Foto Profil</b>
								<input type="file" class="btn btn-primary" id="upload_image">
								<div class="image_demo" style="width:350px; margin-top:30px"></div>
								<button class="btn btn-primary" id="crop">Unggah Gambar</button>
								<input type="hidden" name="username" id="username" value="{{$pengguna->username}}">
								<input type="hidden" id="url_fotoPengguna" value="{{url('/member/updateFoto/'.$pengguna->username)}}">
							</div>
						</div>
					</div>
					<div class="col-sm-8">

					<div class="table-responsive cart_info">
						<form action="{{url('/member/updateBio/'.$pengguna->username)}}" method="post">
							<input type="hidden" name="_method" value="post">
							{{csrf_field()}}
						<table class="table table-condensed">
							<tbody>
								<tr >
									<td>Username</td>
									<td width="70%">{{$pengguna->username}}</td>
								</tr>
								<tr>
									<td >Nama Lengkap</td>
									<td >{{$pengguna->nama_lengkap}}</td>
								</tr>
								<tr>
									<td>Tanggal Lahir</td>
									<td width="70%">{{$tgl_lahir}}</td>
								</tr>
								<tr>
									<td>Jenis Kelamin</td>
									<td width="70%">
										<input type="radio" value="1" name="jenis_kelamin" {{$pengguna->jenis_kelamin == 1?"checked":""}} > Laki-laki
										&nbsp;&nbsp;
										<input type="radio" value="2" name="jenis_kelamin" {{$pengguna->jenis_kelamin == 2?"checked":""}} > Perempuan
									</td>
								</tr>
								<tr>
									<td>NO HP</td>
									<td width="70%">
										<input type="text" name="no_hp" value="{{$pengguna->no_hp}}" class="form-control" maxlength="15" placeholder="No HP" onkeypress='return numeric(event);'>
										@if($errors->first('no_hp')<>null)
				                            <div class="alert alert-danger">* {{ $errors->first('no_hp') }}</div>
				                        @endif
									</td>
								</tr>
								<tr>
									<td>Alamat Asli</td>
									<td width="70%">
										<input type="text" name="alamat_pengguna" value="{{$pengguna->alamat_pengguna}}" class="form-control" placeholder="Alamat Asli">
										@if($errors->first('alamat_pengguna')<>null)
				                            <div class="alert alert-danger">* {{ $errors->first('alamat_member') }}</div>
				                        @endif
									</td>
								</tr>

								<tr>
									<td>Nama Toko</td>
									<td width="70%">
										<input type="text" name="nama_toko" value="{{$pengguna->nama_toko}}" class="form-control" placeholder="Nama Toko">
										@if($errors->first('nama_toko')<>null)
				                            <div class="alert alert-danger">* {{ $errors->first('nama_toko') }}</div>
				                        @endif
									</td>
								</tr>
                <tr>
									<td>Alamat Toko</td>
									<td width="70%">
										<input type="text" name="alamat_toko" value="{{$pengguna->alamat_toko}}" class="form-control" placeholder="Alamat Toko">
										@if($errors->first('alamat_toko')<>null)
				                            <div class="alert alert-danger">* {{ $errors->first('alamat_toko') }}</div>
				                        @endif
									</td>
								</tr>
								<tr>
									<td></td>
									<td width="70%">
										<input type="submit" value="SIMPAN" class="btn btn-primary">
									</td>
								</tr>
							</tbody>
						</table>
						</form>
					</div>


					</div>

				</div>

				<div class="tab-pane fade" id="profiltoko" >
					<div class="container">
						<div class="row">
							<div class="col-sm-11" style="margin:auto;">
								<button class="btn btn-primary" id="btn_addProduk"><span class="fa fa-plus"></span> TAMBAH PRODUK</button>
								<br><br>
								<div class="cover_frm_produk" style="padding-bottom: 50px;">
								 	<div class="table-responsive cart_info">
								 		<div class="fb_frm_produk">
								 		</div>
								 	{{Form::open(array('url'=>'/add_produk','method'=>'post','id'=>'frm_produk'))}}
								 	<table class="table table-condensed">
								 		<tbody>
								 			<tr>
								 				<td>Nama Produk</td>
								 				<td>
								 					{{Form::text('nama_produk','',array('class'=>'form-control','placeholder'=>'Nama Produk'))}}
								 				</td>
								 			</tr>
								 			<tr>
								 				<td>Harga Produk</td>
								 				<td>
                          <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Harga</label>
                            <div class="input-group">
                              <div class="input-group-addon">Rp.</div>
                              {{Form::text('harga_produk','',array('class'=>'form-control','placeholder'=>'Harga Produk', 'onkeypress' => 'return numeric(event)'))}}
                              <div class="input-group-addon">.-</div>
                            </div>
                          </div>
								 				</td>
								 			</tr>
								 			<tr>
								 				<td >Kategori Produk</td>
								 				<td>
								 					{{Form::select('kategori_produk',$kategori,'0', ['class' => 'form-control select2', 'style' => 'width: 100%;'])}}
								 				</td>
								 			</tr>
								 			<tr>
								 				<td style="vertical-align: top;">Deskripsi Produk</td>
								 				<td>
								 					{{Form::textarea('deskripsi_produk','',array('class'=>'form-control editor','cols'=>'50','rows'=>'3','id'=>'keterangan_produk'))}}
								 				</td>
								 			</tr>
								 			<tr>
								 				<td></td>
								 				<td>{{Form::submit('LANJUTKAN',array('class'=>'btn btn-primary'))}}</td>
								 			</tr>
								 		</tbody>
								 	</table>
								   	{{Form::close()}}
									</div>
								</div>
								<div id="cover_upload_gambar_produk" style="padding-bottom: 50px;">
									<center>
									<b>Unggah Gambar Produk</b>
									<input type="file" class="btn btn-primary" id="upload_image_produk">
									<div class="image_demo_produk" style="width:350px; margin-top:30px"></div>
									<input type="hidden" id="url_fotoProduk" value="{{url('/insertGambarProduk')}}">
									<button class="btn btn-primary" id="crop_gambar_produk">SELESAI</button>
									</center>
								</div>
								<table id="list_produk" class="table table-striped table-bordered" style="width:100%">
							        <thead>
							            <tr>
							                <th>NAMA PRODUK</th>
							                <th>HARGA PRODUK</th>
							                <th>KATEGORI</th>
							                <th>TANGGAL INPUT</th>
							                <th>AKSI</th>
							            </tr>
							        </thead>
							        <tbody>
										@foreach($produk as $produk)
							            <tr>
							                <td>{{$produk->nama_produk}}</td>
							                <td>Rp. {{ number_format($produk->harga_produk,0,'','.') }},-</td>
							                <td>{{$produk->nama_kategori}}</td>
							                <td>{{$tgl_pro[$produk->kode_produk]}}</td>
							                <td>
							                	<center>

							                	<a href="{{url('/editproduk/'.$produk->kode_produk)}}" class="btn btn-success" style="padding-left: 10px;"><span class="fa fa-pencil"></span> Edit</a>
                                <a href="{{url('/deleteproduk/'.$produk->kode_produk)}}" class="btn btn-danger" onclick="hapus_produk('btn{{ $produk->kode_produk }}')" id="btn{{ $produk->kode_produk }}"><span class="fa fa-trash-o"></span> Hapus</a>
							                	</center>
							                </td>
							            </tr>
										@endforeach
							        </tbody>
							        <tfoot>
							            <tr>
							                <th>NAMA PRODUK</th>
							                <th>HARGA PRODUK</th>
							                <th>KATEGORI</th>
							                <th>TANGGAL INPUT</th>
							                <th>AKSI</th>
							            </tr>
							        </tfoot>
							    </table>
							</div>
						</div>
					</div>




				</div>



			</div>
		</div><!--/category-tab-->



		</div>
	</div>
</div>




</section>



<div id="modal_gantipassword" class="modal fade" role="dialog">
  <div class="modal-dialog">
	{{Form::open(array('url'=>'/member/gantipassword','method'=>'post','id'=>'frm_gantipassword'))}}
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ganti Password</h4>
      </div>
      <div class="modal-body">
        Masukkan Password Lama
        {{Form::password('pass_lama',array('class'=>'form-control'))}}
       	<div id="pass_lamaError" class=" text-danger"></div>
        <br>
       	Masukkan Password Baru
        {{Form::password('pass_baru',array('class'=>'form-control'))}}
        <div id="pass_baruError" class=" text-danger"></div>
        <br>
        <div id="fb_gantipassword"></div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" >Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="modal_editproduk" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus Produk</h4>
      </div>
      <div class="modal-body text-center">
      	<input type="hidden" id="kode_produkHapus">
        <p>Data Yang Terhapus Tidak Akan Bisa Dikembalikan lagi</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default">OK</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





@endsection
