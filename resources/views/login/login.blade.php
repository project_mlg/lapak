@extends('login.Header')

@section('content-Layout')

<section id="form"><!--form-->
    @if (Session::has('info'))
    <div class="row">
      <div class="col-sm-4 col-sm-offset-4">
        <div class="alert alert-info" role="alert">
          {!! session('info') !!}
        </div>
      </div>
    </div>
    @endif
		<div class="row">
			<div class="col-sm-4 col-sm-offset-4">
				<div class="login-form"><!--login form-->
					<h2>Masuk ke Bedag Malang</h2>
					{!! Form::open(['url' => 'in.html', 'method' => 'post']) !!}
            {!! Form::text('username', session('username'), ['class' => 'form-control', 'placeholder' => 'Username', 'autofocus' => 'true']) !!}
						{!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
						{!! Form::submit('Login', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
</section>
@endsection
