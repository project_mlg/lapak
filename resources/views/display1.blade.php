@extends('layout.PenjualanLayout', ['beranda' => '',
                            'produk' => '',
                            'bedag' => 'active'
                            ])

<!-- content-penjualanLayout -->
@section('content-PenjualanLayout')
<section>
	<div class="container" id="content">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="title text-center">Kategori</h2>
			</div>
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">Display</h2>
					@if(count($product )==0)
						<div class="alert alert-info" style="width: 95%;">Produk yang anda cari <b>tidak ditemukan</b></div>
					@else
					@foreach($product as $product)
					<div class="col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
									<div class="productinfo text-center" >
										<div class="foto-produk" style="width: 100%;height: 220px;overflow: hidden;">
											<img width="100%" src="{{asset('images/_produk/'.$product->foto_produk)}}" alt="" />
										</div>
										<h2>Rp{{number_Format($product->harga_produk)}}</h2>
										<p class="teks_produk">{{$product->nama_produk}}</p>
										<a href="{{url('/detailproduk/'.$product->kode_produk)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2>Rp{{number_Format($product->harga_produk)}}</h2>
											<div class="teks_produk">
											<p class="teks_produk">{{$product->nama_produk}}</p>
											</div>
											<a class="btn btn-default add-to-cart" href="{{url('/detailproduk/'.$product->kode_produk)}}"><i class="fa fa-shopping-cart"></i>Detail</a>
										</div>
									</div>
							</div>
							<div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#"><i class="fa fa-home"></i>Bedag <b>{{$product->username}}</b></a></li>
								</ul>
							</div>
						</div>
					</div>
					@endforeach
					@endif

				</div><!--features_items-->
			</div>
<<<<<<< HEAD
		
=======
			<div class="col-sm-12">
				<div class="recommended_items" id="recom"><!--recommended_items-->
					<h2 class="title text-center">recommended items</h2>

					<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">

							<div class="item active">
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/recommend1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>

										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/recommend2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>

										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/recommend3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>

										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/recommend1.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>

										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/recommend2.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>

										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/home/recommend3.jpg" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
						 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						  </a>
						  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
							<i class="fa fa-angle-right"></i>
						  </a>
					</div>
				</div><!--/recommended_items-->
			</div>
>>>>>>> 07c9b8d000c22a0c2b05f5aea8aab8e8bfbd8f35
		</div>
	</div>
</section>
@endsection
<!-- end content-penjualanLayout -->
