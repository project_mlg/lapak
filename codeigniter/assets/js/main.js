/*price range*/

 $('#sl2').slider();

	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};

/*scroll to top*/

$(document).ready(function(){
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});



	$('#uploadfoto_member').click(function(){
	  alert('haha');
	})

  $("#upload_imageEditProduk").click(function(){
    $("#photo_thumb").addClass("animated");
    $("#photo_thumb").addClass("fadeOut");

    setTimeout(function () {
      $("#photo_thumb").addClass("hidden");
        }, 500
    );
  })

  $("#upload_image").click(function(){
    $("#photo_thumb").addClass("animated");
    $("#photo_thumb").addClass("fadeOut");

    setTimeout(function () {
      $("#photo_thumb").addClass("hidden");
        }, 500
    );
  })
});

function changepass(url)
{
  var pass_lama       = $('#pass_lama').val();
  var pass_baru       = $('#pass_baru').val();
  var ulang_pass_baru = $('#ulang_pass_baru').val();

  if(pass_baru == ulang_pass_baru){
    $.ajax({
      url     : url,
      data    : { pass_lama, pass_baru },
      type    : "POST",
      success : function(e){
        pesan('info', e);
      },
      error   : function(e){
        pesan('warning', "Terjadi kesalahan :\n" + e.responseText);
      }
    })
  }else{
    pesan('warning', "Password baru Anda tidak sama! Silahkan ulangi pengisian password.");
  }
}

function pesan(kelas, isi){
  $('#pass_lama').val("");
  $('#pass_baru').val("");
  $('#ulang_pass_baru').val("");

  $("#information").addClass("alert-" + kelas);
  $("#content-info").html(isi);
  $("#information").removeClass("hidden");
  $("#information").addClass("animated");
  $("#information").addClass("fadeIn");
  setTimeout(function () {
    $("#information").addClass("animated");
    $("#information").addClass("fadeOut");
      }, 2000
  );
  setTimeout(function () {
    $("#information").addClass("hidden");
      }, 5500
  );
}

var specialKeys = new Array();
specialKeys.push(8); //Backspace
function numeric(e) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    document.getElementById("error").style.display = ret ? "none" : "none";
    return ret;
}
