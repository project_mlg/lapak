<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('CategoryModel');
        $this->load->library('form_validation');
	      $this->load->library('datatables');
    }

    public function categorydelete()
    {
        if($this->session->userdata('status') == "Admin")
        {
          $kode_kategori    = $this->uri->segment(3);
          $data_kategori    = $this->CategoryModel->get_by_id($kode_kategori);
          $session['info']  = 'Data <strong>'.$data_kategori->nama_kategori.'</strong> telah dihapus!';
          $session['kelas'] = 'warning';

          $this->CategoryModel->delete($kode_kategori);
          $this->session->set_flashdata($session);
          redirect('admin/categorylist');
        }else{
          redirect('main');
        }
    }

    public function store()
    {
        $data['kode_kategori'] = $this->input->post('kode_kategori');
        $data['nama_kategori'] = $this->input->post('nama_kategori');
        if(!empty($data['nama_kategori']))
        {
          if(empty($data['kode_kategori']))
          {
            $session['info']   = 'Data <strong>'.$data['nama_kategori'].'</strong> berhasil disimpan!';
            $this->CategoryModel->insert($data);
          }else{
            $data['updated_at']= date('Y-m-d H:i:s');
            $session['info']   = 'Data <strong>'.$data['nama_kategori'].'</strong> berhasil diperbarui!';
            $this->CategoryModel->update($data['kode_kategori'], $data);
          }
          $session['kelas']    = 'info';
          $this->session->set_flashdata($session);
          redirect('admin/categorylist');
        }else{
          redirect('main');
        }
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->CategoryModel->json();
    }

    public function read($id)
    {
        $row = $this->CategoryModel->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_kategori' => $row->kode_kategori,
		'nama_kategori' => $row->nama_kategori,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('categorycontroller/categories_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('categorycontroller'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('categorycontroller/create_action'),
	    'kode_kategori' => set_value('kode_kategori'),
	    'nama_kategori' => set_value('nama_kategori'),
	    'created_at' => set_value('created_at'),
	    'updated_at' => set_value('updated_at'),
	);
        $this->load->view('categorycontroller/categories_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_kategori' => $this->input->post('nama_kategori',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->CategoryModel->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('categorycontroller'));
        }
    }

    public function update($id)
    {
        $row = $this->CategoryModel->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('categorycontroller/update_action'),
		'kode_kategori' => set_value('kode_kategori', $row->kode_kategori),
		'nama_kategori' => set_value('nama_kategori', $row->nama_kategori),
		'created_at' => set_value('created_at', $row->created_at),
		'updated_at' => set_value('updated_at', $row->updated_at),
	    );
            $this->load->view('categorycontroller/categories_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('categorycontroller'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_kategori', TRUE));
        } else {
            $data = array(
		'nama_kategori' => $this->input->post('nama_kategori',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->CategoryModel->update($this->input->post('kode_kategori', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('categorycontroller'));
        }
    }

    public function delete($id)
    {
        $row = $this->CategoryModel->get_by_id($id);

        if ($row) {
            $this->CategoryModel->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('categorycontroller'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('categorycontroller'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama_kategori', 'nama kategori', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('updated_at', 'updated at', 'trim|required');

	$this->form_validation->set_rules('kode_kategori', 'kode_kategori', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file CategoryController.php */
/* Location: ./application/controllers/CategoryController.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-06 14:02:04 */
/* http://harviacode.com */
