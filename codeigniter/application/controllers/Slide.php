<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slide extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('SlideModel');
        $this->load->library('form_validation');
	      $this->load->library('datatables');
    }

    public function store()
    {
      $data = $request->image;
      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);
      $data = base64_decode($data);
      $image_name= date("Ymdhis") .'.png';
      $path = public_path() . "/images/slide/" . $image_name;

      file_put_contents($path, $data);

      Slide::create(['slide'=>$image_name]);
      $feedback['url'] = url('/admin/slidelist.html');
      return $feedback;
    }

    public function slidedelete()
    {
      if($this->session->userdata('status') == "Admin")
      {
        $id               = $this->uri->segment(3);
        $data_slide       = $this->SlideModel->get_by_id($id);
        $session['info']  = 'Data <strong>'.$data_slide->judul_slide.'</strong> telah dihapus!';
        $session['kelas'] = 'warning';

        $this->Custom->deleteimage($data_slide->slide, 'slide');
        $this->SlideModel->delete($id);
        $this->session->set_flashdata($session);
        redirect('admin/slidelist');
      }else{
        redirect('main');
      }
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->SlideModel->json();
    }

    public function read($id)
    {
        $row = $this->SlideModel->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'judul_slide' => $row->judul_slide,
		'slide' => $row->slide,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('slidecontroller/slides_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('slidecontroller'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('slidecontroller/create_action'),
	    'id' => set_value('id'),
	    'judul_slide' => set_value('judul_slide'),
	    'slide' => set_value('slide'),
	    'created_at' => set_value('created_at'),
	    'updated_at' => set_value('updated_at'),
	);
        $this->load->view('slidecontroller/slides_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'judul_slide' => $this->input->post('judul_slide',TRUE),
		'slide' => $this->input->post('slide',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->SlideModel->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('slidecontroller'));
        }
    }

    public function update($id)
    {
        $row = $this->SlideModel->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('slidecontroller/update_action'),
		'id' => set_value('id', $row->id),
		'judul_slide' => set_value('judul_slide', $row->judul_slide),
		'slide' => set_value('slide', $row->slide),
		'created_at' => set_value('created_at', $row->created_at),
		'updated_at' => set_value('updated_at', $row->updated_at),
	    );
            $this->load->view('slidecontroller/slides_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('slidecontroller'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'judul_slide' => $this->input->post('judul_slide',TRUE),
		'slide' => $this->input->post('slide',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->SlideModel->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('slidecontroller'));
        }
    }

    public function delete($id)
    {
        $row = $this->SlideModel->get_by_id($id);

        if ($row) {
            $this->SlideModel->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('slidecontroller'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('slidecontroller'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('judul_slide', 'judul slide', 'trim|required');
	$this->form_validation->set_rules('slide', 'slide', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('updated_at', 'updated at', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file SlideController.php */
/* Location: ./application/controllers/SlideController.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-06 14:02:52 */
/* http://harviacode.com */
