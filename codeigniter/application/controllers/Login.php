<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
	      $this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('login/main');
    }

    public function signin()
    {
      if(!empty($this->input->post('username')))
      {
        $cek['username']      = $this->input->post('username');
        $password             = $this->Custom->encry($this->input->post('password'));
        $data_pengguna        = $this->PenggunaModel->get_by_id($cek['username']);

        if(!$data_pengguna)
        {
          $this->session->set_flashdata('status', 'Username tidak terdaftar!');
          redirect('login');
        }else{
          if($data_pengguna->password == $password)
          {
            $cek['foto']         = $this->Custom->photo($data_pengguna->foto, 'pengguna', "");
            $cek['nama_lengkap'] = $data_pengguna->nama_lengkap;
            $cek['status']       = $data_pengguna->status;
            $this->session->set_userdata($cek);

            if($cek['status'] <> 'Admin'){
                redirect('main/pengguna/'.$cek['username']);
            }else{
                redirect('admin');
            }
          }else{
            $this->session->set_flashdata('status', 'Password anda salah!');
            $this->session->set_flashdata('username', $cek['username']);
            redirect('login');
          }
        }
      }else{
        redirect('login');
      }
    }

    public function signout()
  	{
  		$this->session->sess_destroy();
  		redirect('login');
  	}
}
