<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ProductModel');
        $this->load->library('form_validation');
	      $this->load->library('datatables');
    }

    public function productinfo()
    {
        if(!empty($this->input->post('kode_produk'))){
          $product  = $this->ProductModel->get_by_id($this->input->post('kode_produk'));
          $foto     = $this->Custom->photo($product->foto_produk, '_produk', '');
          $data     = $product->nama_produk."|".
                      $product->username."|".
                      $product->nama_toko."|".
                      $product->deskripsi_produk."|Rp. ".
                      number_format($product->harga_produk,0,'','.').",-|".
                      $product->nama_kategori."|".
                      $foto."|";
          echo $data;
        }else{
          redirect('main');
        }
    }

    public function productdelete()
    {
        if($this->session->userdata('status') == "Admin")
        {
          $kode_produk      = $this->uri->segment(3);
          $data_produk      = $this->ProductModel->get_by_id($kode_produk);
          $session['info']  = 'Data <strong>'.$data_produk->nama_produk.'</strong> telah dihapus!';
          $session['kelas'] = 'warning';

          $this->ProductModel->delete($kode_produk);
          $this->session->set_flashdata($session);
          redirect('admin/productlist');
        }else{
          redirect('main');
        }
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->ProductModel->json();
    }

    public function read($id)
    {
        $row = $this->ProductModel->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_produk' => $row->kode_produk,
		'nama_produk' => $row->nama_produk,
		'username' => $row->username,
		'deskripsi_produk' => $row->deskripsi_produk,
		'harga_produk' => $row->harga_produk,
		'kode_kategori' => $row->kode_kategori,
		'foto_produk' => $row->foto_produk,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('productcontroller/products_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('productcontroller'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('productcontroller/create_action'),
	    'kode_produk' => set_value('kode_produk'),
	    'nama_produk' => set_value('nama_produk'),
	    'username' => set_value('username'),
	    'deskripsi_produk' => set_value('deskripsi_produk'),
	    'harga_produk' => set_value('harga_produk'),
	    'kode_kategori' => set_value('kode_kategori'),
	    'foto_produk' => set_value('foto_produk'),
	    'created_at' => set_value('created_at'),
	    'updated_at' => set_value('updated_at'),
	);
        $this->load->view('productcontroller/products_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_produk' => $this->input->post('nama_produk',TRUE),
		'username' => $this->input->post('username',TRUE),
		'deskripsi_produk' => $this->input->post('deskripsi_produk',TRUE),
		'harga_produk' => $this->input->post('harga_produk',TRUE),
		'kode_kategori' => $this->input->post('kode_kategori',TRUE),
		'foto_produk' => $this->input->post('foto_produk',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->ProductModel->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('productcontroller'));
        }
    }

    public function update($id)
    {
        $row = $this->ProductModel->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('productcontroller/update_action'),
		'kode_produk' => set_value('kode_produk', $row->kode_produk),
		'nama_produk' => set_value('nama_produk', $row->nama_produk),
		'username' => set_value('username', $row->username),
		'deskripsi_produk' => set_value('deskripsi_produk', $row->deskripsi_produk),
		'harga_produk' => set_value('harga_produk', $row->harga_produk),
		'kode_kategori' => set_value('kode_kategori', $row->kode_kategori),
		'foto_produk' => set_value('foto_produk', $row->foto_produk),
		'created_at' => set_value('created_at', $row->created_at),
		'updated_at' => set_value('updated_at', $row->updated_at),
	    );
            $this->load->view('productcontroller/products_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('productcontroller'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_produk', TRUE));
        } else {
            $data = array(
		'nama_produk' => $this->input->post('nama_produk',TRUE),
		'username' => $this->input->post('username',TRUE),
		'deskripsi_produk' => $this->input->post('deskripsi_produk',TRUE),
		'harga_produk' => $this->input->post('harga_produk',TRUE),
		'kode_kategori' => $this->input->post('kode_kategori',TRUE),
		'foto_produk' => $this->input->post('foto_produk',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->ProductModel->update($this->input->post('kode_produk', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('productcontroller'));
        }
    }

    public function delete($id)
    {
        $row = $this->ProductModel->get_by_id($id);

        if ($row) {
            $this->ProductModel->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('productcontroller'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('productcontroller'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama_produk', 'nama produk', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('deskripsi_produk', 'deskripsi produk', 'trim|required');
	$this->form_validation->set_rules('harga_produk', 'harga produk', 'trim|required');
	$this->form_validation->set_rules('kode_kategori', 'kode kategori', 'trim|required');
	$this->form_validation->set_rules('foto_produk', 'foto produk', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('updated_at', 'updated at', 'trim|required');

	$this->form_validation->set_rules('kode_produk', 'kode_produk', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file ProductController.php */
/* Location: ./application/controllers/ProductController.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-06 14:02:36 */
/* http://harviacode.com */
