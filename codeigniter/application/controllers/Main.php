<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller
{
    public function index()
    {
      $data['product']   = $this->ProductModel->get_latest();
      $data['categorie'] = $this->CategoryModel->get_all();
      $data['foto']      = array();
      $data['pro_cat']   = array();
      $data['foto_cat']  = array();
    	$data['slide']     = $this->SlideModel->get_all();

      foreach ($data['product'] as $produk) {
        $data['foto'][$produk->kode_produk]          = $this->Custom->photo($produk->foto_produk, '_produk', "");
      }

      foreach ($data['categorie'] as $category) {
        $data['pro_cat'][$category->kode_kategori]   = $this->ProductModel->get_by_category($category->kode_kategori);

        foreach ($data['pro_cat'][$category->kode_kategori] as $photo_cat) {
          $data['foto_cat'][$photo_cat->kode_produk] = $this->Custom->photo($photo_cat->foto_produk, '_produk', "");
        }
      }

    	$this->load->view('main/Home', $data);
    }

    public function produk()
    {
      $data['username']     = $this->uri->segment(3);
      $data['kategori']     = $this->input->get('kategori');
      $data['cari']         = $this->input->get('cari');
      $data['page']         = base_url('main/produk/'.$data['username']);
      $base                 = base_url(uri_string());
      $page                 = $this->input->get('page');

      if(!empty($data['kategori']))
      {
        $base = $base."?kategori=".$data['kategori'];
      }

      if(!empty($data['cari']))
      {
          $base = $base."?kategori=".$data['kategori']."&cari=".$data['cari'];
      }

      if($data['username'] == 'all')
      {
        $all                = $this->ProductModel->num_rows_by_category($data['kategori'], $data['cari'], "");
        $data['product']    = $this->ProductModel->get_all_by_category($data['kategori'], $data['cari'], "", $page);
        $data['nama_bedag'] = 'Semua Produk';
      }else{
        $all                = $this->ProductModel->num_rows_by_category($data['kategori'], $data['cari'], $data['username']);
        $data['product']    = $this->ProductModel->get_all_by_category($data['kategori'], $data['cari'], $data['username'], $page);
        $data['nama_bedag'] = $this->PenggunaModel->get_by_id($data['username'])->nama_toko;
      }

      $data['pagination']   = $this->Custom->pagination($all, $base, $page);
      $data['foto']         = array();
      $data['categorie']    = $this->CategoryModel->get_all();
      $data['bedag']        = $this->ProductModel->get_num_product();

      foreach ($data['product'] as $produk) {
        $data['foto'][$produk->kode_produk]          = $this->Custom->photo($produk->foto_produk, '_produk', "");
      }

      $this->load->view('main/Produk', $data);
    }

    public function daftarbedag()
    {
      $data['daftarbedag'] = $this->PenggunaModel->except_admin();
      $data['categorie']   = $this->CategoryModel->get_all();
      $this->load->view('main/DaftarBedag',$data);
    }

    public function detailproduk()
    {
      $kode_produk        = $this->uri->segment(3);
      $data['product']    = $this->ProductModel->get_by_id($kode_produk);
      $data['categorie']  = $this->CategoryModel->get_all();
      $data['foto']       = $this->Custom->photo($data['product']->foto_produk, '_produk', '');

      $this->load->view('main/ProdukDetail',$data);
    }

    public function pengguna()
    {
      if(empty($this->session->userdata('username')))
      {
          redirect('login');
      }

      $username          = $this->session->userdata('username');
      $data['pengguna']  = $this->PenggunaModel->get_by_id($username);
      $data['produk']    = $this->ProductModel->get_by_username($username);
      $data['categorie']  = $this->CategoryModel->get_all();
      $tanggal           = date_create($data['pengguna']->tgl_lahir);
      $data['tgl_lahir'] = date_format($tanggal, "d M Y");
      $data['foto']      = $this->Custom->photo($data['pengguna']->foto, 'pengguna', "");
      $data['tgl_pro']   = array();
      $data['kategori']  = array();

      foreach ($data['produk'] as $product) {
        $tgl_produk     = date_create($product->created_at);
        $data['tgl_pro'][$product->kode_produk] = date_format($tgl_produk, "H:i:s, d M Y");
      }

      foreach ($data['categorie'] as $category) {
        $data['kategori'][$category->kode_kategori] = $category->nama_kategori;
      }

      //print_r($data['foto']);
      $this->load->view('main/Member',$data);
    }

    public function editproduk()
    {
      $kode_produk         = $this->uri->segment(3);
      $data['product']     = $this->ProductModel->get_by_id($kode_produk);
      $data['categorie']   = $this->CategoryModel->get_all();
      $data['foto']        = $this->Custom->photo($data['product']->foto_produk, '_produk', "");

      $this->load->view('main/ProdukEdit', $data);
    }
}
