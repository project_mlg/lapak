<?php $this->load->view('main/HeadLayout') ?>

<section>
<div class="container">
	<div class="row">
	<div class="col-lg-12">
		<div class="product-details">
			<div class="col-sm-4">
				<div class="view-product">
					<img src="<?= $foto ?>" alt="<?= $product->nama_produk ?>" />
				</div>
				<!-- <div id="similar-product" >
						  <a href="#!"><img class="img img-thumbnail" src="<?= $foto ?>" alt="<?= $product->nama_produk ?>"></a>
				</div> -->

			</div>
			<div class="col-sm-8">
				<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#details" data-toggle="tab">Detail</a></li>
								<li><a href="#profiltoko" data-toggle="tab">Profil Toko</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<div class="row">
									<div class="col-md-8">
										<h2><?= $product->nama_produk ?></h2>
									</div>
									<div class="col-md-4">
										<span>
											<h3>Rp. <?= number_format($product->harga_produk,0,'','.') ?>,-</h3>
										</span>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<b>Deskripsi Produk : </b><br>
										<?= $product->deskripsi_produk ?>
										<p style="margin-top: 40px"><b>*Catatan</b></p>
										<p>Untuk pemesanan silahkan cek kontak penjual pada tab Profil Toko</p>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="profiltoko" >
								<div class="col-sm-12">
									<table class="table table-responsive table-stripped">
										<tr>
											<td>Nama Bedag</td>
											<td><?=$product->nama_toko?></td>
										</tr>
										<tr>
											<td>Alamat Toko</td>
											<td><?=$product->alamat_toko?></td>
										</tr>
										<tr>
											<td>Alamat Pemilik Toko</td>
											<td><?=$product->alamat_pengguna?></td>
										</tr>
										<tr>
											<td>Telp</td>
											<td><?=$product->no_hp?></td>
										</tr>
									</table>
									<p><b>*Catatan</b></p>
									<p>Untuk pemesanan silahkan hubungi nomor telpon yang tertera</p>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
<?php $this->load->view('main/FootLayout') ?>
