<?php $this->load->view('main/HeadLayout') ?>
<section>
	<div class="container" id="content">
		<div class="row">
      <div class="col-sm-3">
        <div class="left-sidebar">
          <h2>Kategori</h2>
          <div class="panel-group category-products" id="accordian">
            <?php foreach($categorie as $category) {?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><a href="<?= $page.'?kategori='.$category->nama_kategori ?>"><?= $category->nama_kategori ?></a></h4>
              </div>
            </div>
            <?php } ?>
          </div>

          <div class="brands_products">
            <h2>Bedag</h2>
            <div class="brands-name">
              <ul class="nav nav-pills nav-stacked">
                <?php foreach($bedag as $shop){ ?>
                  <li><a href="<?= base_url('main/produk/'.$shop->username.'.html') ?>"> <span class="pull-right">(<?= $shop->banyak ?>)</span><?= $shop->nama_toko ?></a></li>
                <?php } ?>
              </ul>
            </div>
          </div>

        </div>
      </div>
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">Display</h2>
          <?php if(!empty($cari)) { ?>
						<center><div class="alert alert-info" style="width: 95%;">
              Hasil pencarian dari <strong><?= $cari ?></strong>
              <?= (!empty($kategori) ? ' dengan kategori '.$kategori : '') ?>
              <?= ($nama_bedag <> "Semua Produk" ? ' di toko '.$nama_bedag : '') ?>
            </div></center>
					<?php } ?>
					<?php if(count($product )==0) { ?>
						<center><div class="alert alert-info" style="width: 95%;">Produk yang anda cari <b>tidak ditemukan</b></div></center>
					<?php }
					foreach($product as $product) { ?>
					<div class="col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
									<div class="productinfo text-center" >
										<div class="foto-produk" style="width: 100%;height: 220px;overflow: hidden;">
											<img width="100%" src="<?= base_url('assets/images/_produk/'.$product->foto_produk) ?>" alt="" />
										</div>
										<h2>Rp<?=number_Format($product->harga_produk)?></h2>
										<p class="teks_produk"><?=$product->nama_produk?></p>
										<a href="<?=base_url('main/detailproduk/'.$product->kode_produk.'.html')?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2>Rp<?=number_Format($product->harga_produk)?></h2>
											<div class="teks_produk">
											<p class="teks_produk"><?=$product->nama_produk?></p>
											</div>
											<a class="btn btn-default add-to-cart" href="<?= base_url('main/detailproduk/'.$product->kode_produk.'.html') ?>"><i class="fa fa-shopping-cart"></i>Detail</a>
										</div>
									</div>
							</div>
							<div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="<?= base_url('main/produk/'.$product->username.'.html') ?>"><i class="fa fa-home"></i>Bedag <b><?=$product->username?></b></a></li>
								</ul>
							</div>
						</div>
					</div>
        <?php } ?>

				</div><!--features_items-->

				<center><?= $pagination ?></center>
			</div>
		</div>
	</div>
</section>

<?php $this->load->view('main/FootLayout') ?>
