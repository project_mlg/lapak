<?php $this->load->view('main/HeadLayout') ?>

<section id="slider">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="slider-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<?php for($i=0;$i<count($slide);$i++) {?>
						<li data-target="#slider-carousel" data-slide-to="<?= $i ?> " class="<?= $i == 0 ? 'active' : '' ?>"></li>
            <?php } ?>
					</ol>

					<div class="carousel-inner">
						<?php $no=1;
						foreach($slide as $slide2) { ?>
						<div class="item <?= $no==1 ? 'active':'' ?>">
							<img src="<?= base_url('assets/images/slide/'.$slide2->slide) ?>" class="img img-responsive" width="100%" alt="">
						</div>
						<?php $no++; }?>
					</div>

					<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>

			</div>
		</div>
	</div>
</section>

<section>
	<div class="container" id="content">
		<div class="row">
			<div class="col-sm-12 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">Display</h2>
					<?php if(count($product )==0){ ?>
						<div class="alert alert-info" style="width: 95%;"><center>Tidak ada produk yang bisa ditampilkan</center></div>
					<?php }
					foreach($product as $product){ ?>
					<div class="col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
									<div class="productinfo text-center"  >
										<div class="foto-produk">
											<img width="100%" src="<?= $foto[$product->kode_produk] ?>" alt="<?= $product->nama_produk ?>" />
										</div>
										<h2>Rp.  <?= number_format($product->harga_produk,0,'','.') ?>,-</h2>
										<p class="teks_produk"><?=$product->nama_produk?></p>
										<a href="<?= base_url('main/detailproduk/'.$product->kode_produk.'.html') ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2>Rp.  <?= number_format($product->harga_produk,0,'','.') ?>,-</h2>
											<p class="teks_produk"><?=$product->nama_produk?></p>
											<a href="<?= base_url('main/detailproduk/'.$product->kode_produk.'.html') ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
										</div>
									</div>
							</div>
							<div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#"><i class="fa fa-home"></i>Bedag <b><?=$product->username?></b></a></li>
								</ul>
							</div>
						</div>
					</div>
        <?php } ?>
					<center style="padding-bottom: 40px"><a href="<?= base_url('main/produk/all.html') ?>" class="title text-center"> Lihat lainnya </a></center>
				</div><!--features_items-->

				<div class="category-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<?php $i = "active";
							foreach($categorie as $kategori) {?>
								<li class="<?= $i ?>"><a href="#tab_<?= $kategori->kode_kategori ?>" data-toggle="tab"><?= $kategori->nama_kategori ?></a></li>
								<?php $i = ""; }?>
						</ul>
					</div>
					<div class="tab-content">
						<?php $i = "active in";
						foreach($categorie as $kategori) {?>
						<div class="tab-pane fade <?= $i ?>" id="tab_<?= $kategori->kode_kategori ?>" >
							<?php if(count($pro_cat[$kategori->kode_kategori] )==0){ ?>
								<div class="col-sm-12">
									<center><div class="alert alert-info" style="width: 95%;"><center>Tidak ada produk yang bisa ditampilkan</center></div></center>
								</div>
							<?php }?>
							<?php foreach($pro_cat[$kategori->kode_kategori] as $product_cat){ ?>
							<div class="col-sm-3">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
                      <div class="foto-produk">
                        <img src="<?= $foto_cat[$product_cat->kode_produk] ?>" alt="<?= $product->nama_produk ?>" />
                      </div>
											<h2>Rp.  <?= number_format($product_cat->harga_produk,0,'','.') ?>,-</h2>
											<p><?= $product_cat->nama_produk ?></p>
											<a href="<?= base_url('main/detailproduk/'.$product_cat->kode_produk.'.html') ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
										</div>

									</div>
								</div>
							</div>
            <?php } ?>
						</div>
						<?php $i = ""; } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $this->load->view('main/FootLayout') ?>
