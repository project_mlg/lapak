<?php
  $menu = $this->uri->segment(2);
  $toko = $this->uri->segment(3);
  if($menu <> 'produk'){
    $url = base_url('main/produk/all');
  }else{
    $url = base_url('main/produk/'.$toko);
  }

  $beranda = ""; $produk = ""; $bedag = "";

  switch ($menu) {
    case 'produk'       : $produk  = "active"; break;
    case 'detailproduk' : $produk  = "active"; break;
    case 'daftarbedag'  : $bedag   = "active"; break;
    case 'pengguna'     :                    ; break;
    case 'editproduk'   :                    ; break;
    default             : $beranda = "active"; break;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BEDAGMALANG</title>

    <link href="<?= base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/prettyPhoto.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/price-range.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/animate.css')?>" rel="stylesheet">
  	<link href="<?= base_url('assets/css/main.css')?>" rel="stylesheet">
  	<link href="<?= base_url('assets/css/responsive.css')?>" rel="stylesheet">
	  <link rel="stylesheet" href="<?= base_url('assets/css/datatables/datatables.css') ?>">
	  <link rel="stylesheet" href="<?= base_url('assets/css/datatables/DataTables-1.10.16/css/dataTables.bootstrap.css') ?>">
  	<link rel="stylesheet" href="<?= base_url('assets/css/croppie.css')?>">
  	<link rel="stylesheet" href="<?= base_url('assets/css/summernote.css')?>">
  	<link href="<?= base_url('assets/css/style.css')?>" rel="stylesheet">


</head>

<body>
	<header id="header"><!--header-->

		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<h3>BEDAG<span>MALANG</span></h3>
						</div>
					</div>
					<div class="col-sm-8">
            <div class="shop-menu pull-right">
            	<ul class="nav navbar-nav">
                <?php if(empty($this->session->userdata('username'))){ ?>
                <li><a href="<?= base_url('/login.html') ?>"><i class="fa fa-lock"></i> Login</a></li>
                <?php }else{ ?>
            		<li><a href="<?= base_url('/main/pengguna.html') ?>"><i class="fa fa-user"></i><?= $this->session->userdata('nama_lengkap') ?></a></li>
            		<li><a href="<?= base_url('/login/signout.html') ?>"><i class="fa fa-lock"></i> Sign Out</a></li>
                <?php } ?>
            	</ul>
            </div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
          <div class="col-sm-8">
          	<div class="navbar-header">
          		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          			<span class="sr-only">Toggle navigation</span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          		</button>
          	</div>
          	<div class="mainmenu pull-left">
          		<ul class="nav navbar-nav collapse navbar-collapse">
          			<li ><a href="<?= base_url() ?>" class="<?= $beranda ?>">Beranda</a></li>
          			<li class="dropdown"><a href="#" class="<?= $produk ?>">Kategori<i class="fa fa-angle-down"></i></a>
                          <ul role="menu" class="sub-menu">
          										<li><a href="<?= base_url('main/produk/all.html' ) ?>">Semua Produk</a></li>
                      					<?php foreach($categorie as $kategori){ ?>
                              <li><a href="<?= base_url('main/produk/all?kategori='.$kategori->nama_kategori ) ?>"><?= $kategori->nama_kategori ?></a></li>
                                <?php } ?>
                          </ul>
                      </li>
          			<li><a href="<?= base_url('main/daftarbedag.html') ?>" class="<?= $bedag ?>">Daftar Bedag Malang</a></li>
          		</ul>
          	</div>
          </div>

          <div class="col-sm-4">
          	<div class="search_box pull-right">
          		<form action="<?= $url ?>" method="get">
          		<input type="hidden" name="kategori" value="<?php if(isset($_GET['kategori'])){ echo $_GET['kategori']; } ?>">
          		<input type="text" name="cari" placeholder="Cari Nama Produk"/>
          		</form>
          	</div>
          </div>
				</div>
			</div>
		</div>
	</header>
