<?php $this->load->view('admin/Sidebar') ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Slide
      <small>Daftar</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Slide</li>
    </ol>
  </section>

  <section class="content">
    <?php if(!empty($this->session->flashdata('info'))){ ?>
    <div class="callout callout-<?= $this->session->flashdata('kelas') ?> hidden" id="information">
        <h4>Informasi</h4>
        <?= $this->session->flashdata('info') ?>
    </div>
    <?php } ?>

    <div class="row">
      <section class="col-md-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-users"></i>

            <h3 class="box-title">Slide</h3>
            <div class="pull-right box-tools">
              <a href="<?= base_url('/admin/slideform/new.html') ?>" type="button" class="btn btn-primary btn-sm" title="Add" data-toggle="tooltip">
                <i class="fa fa-plus"></i> Tambah Slide</a>
              <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Slide</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                    foreach($slidelist as $slide) {
                  ?>
                    <tr>
                        <td><?= $no ?></td>
                        <td><?= $slide->judul_slide ?></td>
                        <td>
                          <a href="#!" data-toggle="modal" data-target="#modal-info" type="button" class="btn btn-primary btn-sm" title="Edit" data-toggle="tooltip" onclick="lihat('<?= base_url('assets/images/slide/'.$slide->slide)?>')">
                            <i class="fa fa-eye"></i> Lihat</a>
                          <button type="button" class="btn btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#modal-warning" onclick="hapus('<?= $slide->judul_slide ?>', '<?= base_url('/slide/slidedelete/'.$slide->id) ?>')">
                            <i class="fa fa-times"></i> Delete</button>
                        </td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
            </table>
          </div>
          <div class="box-footer clearfix">
          </div>
        </div>

      </section>
    </div>
  </section>
</div>

<div class="modal modal-primary fade" id="modal-warning">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Slide <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <p>Apakah benar, data slide <strong><span class="nama"></span></strong> akan dihapus?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
        <a href="<?= base_url('/admin/delete') ?>" class="btn btn-danger" id="btn_delete">Delete</a>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-primary fade" id="modal-info">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"> Lihat Slide <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <img id="img_slide" src="<?= base_url('assets/images/slide/')?>" class="img img-responsive" width="100%" alt="" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>




<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
<?php $this->load->view('admin/FootLayout') ?>
