@extends('admin.Sidebar', ['dashboard' => '',
                            'produk' => '',
                            'kategori' => '',
                            'member' => 'active',
                            'slide' => '',
                            'profil' => '',
                            'password' => ''])

@section('title')
  Member List
@endsection

@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Member
      <small>Daftar</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Member</li>
    </ol>
  </section>

  <section class="content">
    @if (Session::has('info') or count($errors) > 0)
    <div class="callout callout-{{ session('kelas') }} hidden" id="information">
        <h4>Informasi</h4>
        {!! session('info') !!}
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </div>
    @endif

    <div class="row">
      <section class="col-md-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-users"></i>

            <h3 class="box-title">Member</h3>
            <div class="pull-right box-tools">
              <a href="{{ url('/admin/memberform/new.html') }}" type="button" class="btn btn-primary btn-sm" title="Add" data-toggle="tooltip">
                <i class="fa fa-plus"></i> Tambah Member</a>
              <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Nama Lengkap</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  @foreach($memberlist as $member)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $member->username }}</td>
                        <td>{{ $member->nama_lengkap }}</td>
                        <td>{{ $member->status }}</td>
                        <td>
                          <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="Reset Password">
                            <i class="fa fa-undo"></i> Reset</button>
                          <a href="{{ url('/admin/memberform/'.$member->username.'.html') }}" type="button" class="btn btn-warning btn-sm" title="Edit" data-toggle="tooltip">
                            <i class="fa fa-pencil"></i> Edit</a>
                          <button type="button" class="btn btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#modal-warning" onclick="hapus('{{ $member->nama_lengkap }}', '{{ url('/admin/memberdelete/'.$member->username) }}')">
                            <i class="fa fa-times"></i> Delete</button>
                          @if($member->status != "Admin")
                            <a href="{{ url('/admin/logasmember/'.$member->username.'.html') }}" type="button" class="btn btn-primary btn-sm" title="Edit" data-toggle="tooltip">
                              <i class="fa fa-pencil"></i> Log as Member</a>
                          @endif
                        </td>
                    </tr>
                    <?php $no++; ?>
                  @endforeach
                </tbody>
            </table>
          </div>
          <div class="box-footer clearfix">
          </div>
        </div>

      </section>
    </div>
  </section>
</div>

<div class="modal modal-primary fade" id="modal-warning">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Member <span class="nama"></span></h4>
      </div>
      <div class="modal-body">
        <p>Apakah benar, data member <strong><span class="nama"></span></strong> akan dihapus?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
        <a href="{{ url('/admin/delete') }}" class="btn btn-danger" id="btn_delete">Delete</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
@endsection
