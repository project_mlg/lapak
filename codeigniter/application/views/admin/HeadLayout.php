<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BedagMalang | Admin Page</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/ion-icon.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/AdminLTE.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/skins/_all-skins.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/animate.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/iCheck/flat/blue.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/morris/morris.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/jvectormap/jquery-jvectormap-1.2.2.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/datepicker/datepicker3.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/select2/select2.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/daterangepicker/daterangepicker-bs3.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/iCheck/all.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/datatables/datatables.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/datatables/DataTables-1.10.16/css/dataTables.bootstrap.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/dropzone/dist/dropzone.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/croppie.css') ?>">

  <script src="<?= base_url('assets/js/jquery.js') ?>"></script>
  <script src="<?= base_url('assets/js/jquery-ui.js') ?>"></script>
  <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
  <script src="<?= base_url('assets/css/datatables/DataTables-1.10.16/js/jquery.dataTables.js') ?>"></script>
  <script src="<?= base_url('assets/css/datatables/DataTables-1.10.16/js/dataTables.bootstrap.js') ?>"></script>
  <script src="<?= base_url('assets/css/iCheck/icheck.min.js') ?>"></script>
  <script src="<?= base_url('assets/css/select2/select2.full.min.js') ?>"></script>
  <script src="<?= base_url('assets/js/bootstrap3-wysihtml5.all.min.js') ?>"></script>
  <script src="<?= base_url('assets/js/jquery.slimscroll.js') ?>"></script>
  <script src="<?= base_url('assets/js/fastclick.js') ?>"></script>
  <script src="<?= base_url('assets/js/croppie.js') ?>"></script>

</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="<?= base_url('admin.html') ?>" class="logo">
      <span class="logo-mini"><b>B</b>M</span>
      <span class="logo-lg">BEDAG<b>MALANG</b></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= $this->session->userdata('foto') ?>" class="user-image" alt="<?= $this->session->userdata('nama') ?>">
              <span class="hidden-xs"><?= $this->session->userdata('nama_lengkap') ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?= $this->session->userdata('foto') ?>" class="img-circle" alt="<?= $this->session->userdata('username') ?>">
                <p>
                  <?= $this->session->userdata('nama_lengkap') ?>
                  <small><?= $this->session->userdata('status') ?></small>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= base_url('/admin/profile.html') ?>" class="btn btn-default btn-flat">Profil</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url('/login/signout.html') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
