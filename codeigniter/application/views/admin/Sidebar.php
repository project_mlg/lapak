<?php
  $this->load->view('admin/HeadLayout');

  $dashboard = ""; $produk = ""; $kategori = ""; $member = ""; $slide     = ""; $profil = ""; $password = "";
  $menu      = $this->uri->segment(2);

  switch ($menu) {
    case 'productlist' : $produk    = 'active'; break;
    case 'categorylist': $kategori  = 'active'; break;
    case 'memberlist'  : $member    = 'active'; break;
    case 'memberform'  : $member    = 'active'; break;
    case 'slidelist'   : $slide     = 'active'; break;
    case 'profile'     : $profil    = 'active'; break;

    default            : $dashboard = 'active'; break;
  }
?>

  <aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= $this->session->userdata('foto') ?>" class="img-circle" alt="<?= $this->session->userdata('username') ?>">
      </div>
      <div class="pull-left info">
        <p><?= $this->session->userdata('nama_lengkap') ?></p>
        <?= $this->session->userdata('status') ?>
      </div>
    </div>

    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?= $dashboard ?> treeview">
        <a href="<?= base_url('admin.html') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a>
      </li>
      <li class="<?= $produk.$kategori ?> treeview">
        <a href="#">
          <i class="fa fa-dropbox"></i>
          <span>Produk</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $produk ?>"><a href="<?= base_url('/admin/productlist.html') ?>"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
          <li class="<?= $kategori ?>"><a href="<?= base_url('/admin/categorylist.html') ?>"><i class="fa fa-circle-o"></i> Kategori</a></li>
        </ul>
      </li>
      <li class="<?= $member ?>">
        <a href="<?= base_url('/admin/memberlist.html') ?>">
          <i class="fa fa-users"></i> <span>Member</span>
        </a>
      </li>
      <li class="<?= $slide ?>">
        <a href="<?= base_url('/admin/slidelist.html') ?>">
          <i class="fa fa-picture-o"></i> <span>Slide</span>
        </a>
      </li>
      <li class="<?= $profil.$password ?> treeview">
        <a href="#">
          <i class="fa fa-gear"></i>
          <span>Pengaturan Akun</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $profil ?>"><a href="<?= base_url('/admin/profile.html') ?>"><i class="fa fa-circle-o"></i> Profil</a></li>
          <li class="<?= $password ?>"><a href="<?= base_url('/admin/password.html') ?>"><i class="fa fa-circle-o"></i> Password</a></li>
        </ul>
      </li>
      <li>
        <a href="<?= base_url('/login/signout.html') ?>">
          <i class="fa fa-power-off"></i> <span>Sign Out</span>
        </a>
      </li>
    </ul>
  </section>
</aside>
