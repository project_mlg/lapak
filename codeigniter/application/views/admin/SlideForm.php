<?php $this->load->view('admin/Sidebar') ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Slide
      <small>Form</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#!"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?= base_url('/admin/slidelist.html')?>"><i class="fa fa-picture-o"></i> Slide</a></li>
      <li class="active"><a href="#">Form Slide</a></li>
    </ol>
  </section>

  <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Info Slide</h3>
            </div>
              <div class="box-body">
                <input type="file" id="foto_slide">

                <div class="cropfoto_slide" style="width:350px; margin-top:30px"></div>

                <input type="hidden" id="urlfoto_slide" value="<?= base_url('/slide/store.html')?>">
                <button class="btn btn-primary" id="uploadfoto_slide">SIMPAN</button>
              </div>
          </div>
        </div>
      </div>
  </section>
</div>
<script type="text/javascript">
  $(function(){
    $('.select2').select2();
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue'
    })
  });
</script>

<?php $this->load->view('admin/FootLayout') ?>
