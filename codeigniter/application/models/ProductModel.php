<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ProductModel extends CI_Model
{

    public $table = 'products';
    public $id = 'kode_produk';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('kode_produk,nama_produk,username,deskripsi_produk,harga_produk,kode_kategori,foto_produk,created_at,updated_at');
        $this->datatables->from('products');
        //add this line for join
        //$this->datatables->join('table2', 'products.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('productcontroller/read/$1'),'Read')." | ".anchor(site_url('productcontroller/update/$1'),'Update')." | ".anchor(site_url('productcontroller/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kode_produk');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        return $this->db->query("SELECT * FROM `categories`
          INNER JOIN `products` ON `products`.`kode_kategori` =
          `categories`.`kode_kategori` INNER JOIN `penggunas`
          ON `products`.`username` = `penggunas`.`username`")->result();
    }

    // get data by id
    function get_by_id($id)
    {
      return $this->db->query("SELECT * FROM `categories`
        INNER JOIN `products` ON `products`.`kode_kategori` =
        `categories`.`kode_kategori` INNER JOIN `penggunas`
        ON `products`.`username` = `penggunas`.`username` WHERE
        `products`.`kode_produk` = '".$id."'")->row();
    }

    function get_latest()
    {
      $this->db->order_by($this->id, 'desc');
      $this->db->limit(4);
      return $this->db->get($this->table)->result();
    }

    function get_by_category($kode_kategori)
    {
      $this->db->where('kode_kategori', $kode_kategori);
      $this->db->order_by($this->id, 'desc');
      $this->db->limit(4);
      return $this->db->get($this->table)->result();
    }

    function get_all_by_category($category, $cari, $username, $page)
    {

      $products = "SELECT * FROM `categories` INNER JOIN `products` ON `products`.`kode_kategori` = `categories`.`kode_kategori` INNER JOIN
                  `penggunas` ON `products`.`username` = `penggunas`.`username`";
      $where    = " WHERE ";

      if(!empty($category)){
        $products = $products.$where.'categories.nama_kategori = "'.$category.'"';
        $where    = " AND ";
      }

      if(!empty($cari)){
        $products = $products.$where.'products.nama_produk like "%'.$cari.'%"';
        $where    = " AND ";
      }

      if(!empty($username)){
        $products = $products.$where.'products.username = "'.$username.'"';
      }

      if(empty($page)){ $page = 1; }

      $first      = ($page - 1) * 8;
      $products   = $products." ORDER BY products.kode_produk DESC Limit ".$first.", 8";

      return $this->db->query($products)->result();
    }

    function num_rows_by_category($category, $cari, $username)
    {

      $products = "SELECT * FROM `categories` INNER JOIN `products` ON `products`.`kode_kategori` = `categories`.`kode_kategori` INNER JOIN
                  `penggunas` ON `products`.`username` = `penggunas`.`username`";
      $where    = " WHERE ";

      if(!empty($category)){
        $products = $products.$where.'categories.nama_kategori = "'.$category.'"';
        $where    = " AND ";
      }

      if(!empty($cari)){
        $products = $products.$where.'products.nama_produk like "%'.$cari.'%"';
        $where    = " AND ";
      }

      if(!empty($username)){
        $products = $products.$where.'products.username = "'.$username.'"';
      }

      $products   = $products." ORDER BY products.kode_produk DESC";

      return $this->db->query($products)->num_rows();
    }

    function get_num_product()
    {
      return $this->db->query("SELECT penggunas.nama_toko, products.username, Count(kode_produk) AS banyak
        FROM products INNER JOIN penggunas
        ON products.username = penggunas.username
        GROUP BY products.username, penggunas.nama_toko")->result();
    }

    function get_by_username($username)
    {
      return $this->db->query("SELECT * FROM `categories`
        INNER JOIN `products` ON `products`.`kode_kategori` =
        `categories`.`kode_kategori` WHERE
        `products`.`username` = '".$username."'")->result();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_produk', $q);
	$this->db->or_like('nama_produk', $q);
	$this->db->or_like('username', $q);
	$this->db->or_like('deskripsi_produk', $q);
	$this->db->or_like('harga_produk', $q);
	$this->db->or_like('kode_kategori', $q);
	$this->db->or_like('foto_produk', $q);
	$this->db->or_like('created_at', $q);
	$this->db->or_like('updated_at', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_produk', $q);
	$this->db->or_like('nama_produk', $q);
	$this->db->or_like('username', $q);
	$this->db->or_like('deskripsi_produk', $q);
	$this->db->or_like('harga_produk', $q);
	$this->db->or_like('kode_kategori', $q);
	$this->db->or_like('foto_produk', $q);
	$this->db->or_like('created_at', $q);
	$this->db->or_like('updated_at', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file ProductModel.php */
/* Location: ./application/models/ProductModel.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-06 14:02:36 */
/* http://harviacode.com */
